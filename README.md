# Double Descent

Model specifications in PyTorch are in [models](models).

## Full List of Experiments (the Data in the links)

#### Model DD

|Dataset| Architecture  | Optimizer | Comments |Data Link|
|:---:|:---:|:---:|:---:|:---:|
| Cifar10 with 0-20% label noise and data-aug| ResNet18| Adam with LR=0.0001| |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar10 with 0-20% label noise and data-aug| MCNN| SGD with LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar10 with 0-20% label noise and **no data-aug** | MCNN| SGD with LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar100 with 0-20% label noise and data-aug| ResNet18| Adam  with LR=0.0001 | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar100 with 0-20% label noise and data-aug| MCNN| SGD with LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| IWSTL'14| Transformers| SGD with warmup then LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| WMT'14| Transformers| SGD with warmup then LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|


#### Sample DD
|Dataset| Architecture  | Optimizer | Comments |Data Link|
|:---:|:---:|:---:|:---:|:---:|
| IWSTL'14| Transformers| SGD with warmup then LR $`\propto 1/\sqrt{T}`$ |  |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar10 with 20% label noise and varying data size and data-aug | MCNN| SGD with LR $`\propto 1/\sqrt{T}`$ | |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
#### Epoch DD
|Dataset| Architecture  | Optimizer | Comments |Data Link|
|:---:|:---:|:---:|:---:|:---:|
| Cifar10 with 0-20% label noise and data-aug|  ResNet18| Adam with LR=0.0001 |  |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar100 with 0-20% label noise and data-aug|  ResNet18| Adam with LR=0.0001 |  |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
| Cifar10 with 0-20% label noise and data-aug|   MCNN| SGD with LR $`\propto 1/\sqrt{T}`$  |  |[link](https://console.cloud.google.com/storage/browser/preetum/data/cifar10_mcnn/?project=ml-theory&authuser=2)|
